# Snow Awesome JavaScript

O objetivo desta lista é reunir referências de conteúdos técnicos como, bibliotecas, documentações, artigos, cursos, tutoriais, vídeos, livros, podcasts e quaisquer outros conteúdos pertinentes ao aprendizado e aprofundamento de cada tema. 

## Como contribuir

Sugira uma referência apenas se você já a utilizou antes, é importante manter o conteúdo sob curadoria para que essa lista não se torne apenas um *bookmark* de coisas interessantes que você quer salvar para o futuro.

As contribuições devem ser feitas através de *Merge Request* com uma breve explicação do conteúdo e motivação. 

As referências devem seguir o seguinte formato:

```
* [Nome](Link) - Breve explicação, author, ou informação relevante. #tipo 
```

Exemplos:

* [MDN JavaScript](https://developer.mozilla.org/en-US/docs/Web/javascript) - Documentação de JavaScript da MDN. #doc


---

## Linguagem

* [MDN JavaScript](https://developer.mozilla.org/en-US/docs/Web/javascript) - Documentação de JavaScript da MDN. #doc

## Boas Práticas

* [clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript) - Várias mini explicações sobre pontos relevantes do clean aplicado a js. #repo
* [Official Vue Styleguide](https://vuejs.org/v2/style-guide/) - Styleguide oficial do Vue.
* [Vue Extended Styleguide](https://gist.github.com/brianboyko/6e0af86f71da83a82cd4f074363ce046) - Uma "expansão" do styleguide original, feita pro Vue 3 mas tem bastante coisa pra Vue 2 também.
 
## Metaprogramação
## Algoritmos

## Discussões
* [If you don't use typescript, tell me why](https://www.reddit.com/r/javascript/comments/bfsdxl/if_you_dont_use_typescript_tell_me_why/) - Discussão sobre o porquê de (não) usar typescript, com bons argumentos dos dois lados. #discuss #typescript

## Design Patterns
## REST/OpenAPI
## GraphQL
## Sistemas Distribuídos
## Sistemas orientados a Eventos
## Arquitetura
## LGPD
## Gestão de Configuração
## Gestão de Dependências
## Gestão de Credenciais e Certificados
## Gestão de Releases
## Gestão de Infraestrutura
## Gestão de crises
## Métricas
## Monitoramento
## CI/CD
## Testes
## Manutenção de software legado
## Análise Estática de Código
## Análise de Vulnerabilidades
## UI/UX
## Design System
